# Evaluacion

- Para instalar/actualizar los requerimientos del proyecto: framework y librerias:
    ```
    pip install -r requirements/dev.txt
    ```

- Luego proceder a cargar las migraciones:
    ```
    python manage.py migrate
    ```

- Cargar los fixtures en el siguiente orden:
    ejecutar el archivo `./load_fixtures.sh` (agregar los comandos de carga cuando sean necesarios en ese archivo también)

- Es necesario crear un usuario para poder acceder a las APIS.

- Endpoint:
    - http://127.0.0.1:8000/business/create-room/
    - http://127.0.0.1:8000/business/update-room/2/
    - http://127.0.0.1:8000/business/create-events/
    - http://127.0.0.1:8000/business/create-booking/
    - http://127.0.0.1:8000/business/update-booking/1/