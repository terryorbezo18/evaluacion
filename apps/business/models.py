from django.db import models


class Customer(models.Model):
    dni = models.CharField(max_length=20)
    first_name = models.CharField(max_length=200)
    last_name = models.CharField(max_length=200)
    fecha_creacion = models.DateTimeField(auto_now_add=True, blank=True)

    def __str__(self):
        return f"{self.first_name} {self.last_name}"


class Company(models.Model):
    ruc = models.CharField(max_length=20)
    reason_social = models.CharField(max_length=200)
    fecha_creacion = models.DateTimeField(auto_now_add=True, blank=True)

    def __str__(self):
        return self.reason_social


class Room(models.Model):
    description = models.CharField(max_length=150)
    capacity = models.IntegerField()
    state = models.BooleanField(default=True)
    company = models.ForeignKey(Company, on_delete=models.CASCADE)

    def __str__(self):
        return self.description


class TypeEvents(models.TextChoices):
    PUBLIC = "PU", "Public"
    PRIVATE = "PI", "Private"


class Events(models.Model):
    type = models.CharField(
        max_length=2, choices=TypeEvents.choices, default=TypeEvents.PUBLIC
    )
    fecha = models.DateField()
    state = models.BooleanField(default=True)
    room = models.ForeignKey(Room, on_delete=models.CASCADE)

    def __str__(self):
        return str(self.id)


class Booking(models.Model):
    events = models.ForeignKey(Events, on_delete=models.CASCADE)
    customer = models.ForeignKey(Customer, on_delete=models.CASCADE)
    state = models.BooleanField(default=True)
