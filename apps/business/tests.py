from django.test import TestCase

from apps.business.faker import EventsFactory


class TestCreateReserva(TestCase):
    fixtures = ("apps/business/data-company.json", "apps/business/data-customer.json")

    def setUp(self) -> None:
        self.base_url = "/business/create-booking/"
        self.events = EventsFactory()

    def test_create_reserva(self):
        data = {"events": self.events, "customer": 1, "state": True}

        response = self.client.post(
            f"{self.base_url}", data=data, content_type="application/json"
        )
        assert response.status_code == 201
