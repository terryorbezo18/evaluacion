# from rest_framework.serializers import ModelSerializer
from rest_framework import serializers

from apps.business.models import Booking, Events, Room, TypeEvents


class RoomSerializer(serializers.ModelSerializer):
    class Meta:
        model = Room
        fields = ("description", "capacity", "state", "company")


class RoomPutSerializer(serializers.ModelSerializer):
    class Meta:
        model = Room
        fields = ("id", "state")


class EventsSerializer(serializers.ModelSerializer):
    class Meta:
        model = Events
        fields = ("type", "fecha", "state", "room")

    def validate(self, data):
        try:
            validar_creacion_evento = Events.objects.get(
                fecha=data.get("fecha"), room=data.get("room"), state=True
            )
        except Events.DoesNotExist:
            validar_creacion_evento = False
        if validar_creacion_evento:
            raise serializers.ValidationError(
                {"room": ("El sala ya tiene un evento asignado para esa fecha.")}
            )
        return data


class BookingSerializer(serializers.ModelSerializer):
    class Meta:
        model = Booking
        fields = ("events", "customer", "state")

    def validate(self, data):
        if Booking.objects.filter(
            events=data.get("events"), customer=data.get("customer"), state=True
        ):
            raise serializers.ValidationError(
                {"customer": ("El cliente ya tiene una reserva en el evento.")}
            )
        else:
            if data.get("events").type == TypeEvents.PRIVATE:
                validar_evento_tipo = True
            else:
                validar_evento_tipo = False
            if validar_evento_tipo:
                raise serializers.ValidationError({"events": ("El evento es privado.")})
            else:
                cantidad_reserva = Booking.objects.filter(
                    events=data.get("events"), state=True
                ).count()
                if data.get("events").room.capacity == cantidad_reserva:
                    validar_cantidad = True
                else:
                    validar_cantidad = False
                if validar_cantidad:
                    raise serializers.ValidationError(
                        {"events": ("El evento ya no tiene reservas.")}
                    )
        return data


class BookingPutSerializer(serializers.ModelSerializer):
    class Meta:
        model = Booking
        fields = ("id", "state")
