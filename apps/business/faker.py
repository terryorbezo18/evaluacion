from datetime import datetime

import factory.fuzzy

from apps.business.models import Events, Room, TypeEvents


class RoomFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = Room

    description = factory.Faker("description")
    capacity = 2
    state = True
    company = 1


class EventsFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = Events

    type = TypeEvents.PUBLIC
    fecha = factory.fuzzy.FuzzyDate(datetime.date(2022, 1, 1))
    state = True
    room = factory.SubFactory(RoomFactory)
