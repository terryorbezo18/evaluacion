from rest_framework.generics import CreateAPIView, UpdateAPIView

from apps.business.models import Booking, Room
from apps.business.serializers import (BookingPutSerializer, BookingSerializer,
                                       EventsSerializer, RoomPutSerializer,
                                       RoomSerializer)


class RoomAPI(CreateAPIView):
    serializer_class = RoomSerializer


class RoomPutAPI(UpdateAPIView):
    serializer_class = RoomPutSerializer

    def get_queryset(self):
        pk = self.kwargs["pk"]
        return Room.objects.filter(pk=pk)


class EventsAPI(CreateAPIView):
    serializer_class = EventsSerializer


class BookingAPI(CreateAPIView):
    serializer_class = BookingSerializer


class BookingPutAPI(UpdateAPIView):
    serializer_class = BookingPutSerializer

    def get_queryset(self):
        pk = self.kwargs["pk"]
        return Booking.objects.filter(pk=pk)
