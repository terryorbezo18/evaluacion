from django.contrib.auth.decorators import login_required
from django.urls import re_path

from apps.business.views import (BookingAPI, BookingPutAPI, EventsAPI, RoomAPI,
                                 RoomPutAPI)

app_name = "business"

urlpatterns = [
    re_path(r"^create-room/", login_required(RoomAPI.as_view()), name="create-room"),
    re_path(
        r"^update-room/(?P<pk>\d+)/$",
        login_required(RoomPutAPI.as_view()),
        name="update-room",
    ),
    re_path(
        r"^create-events/", login_required(EventsAPI.as_view()), name="create-events"
    ),
    re_path(
        r"^create-booking/", login_required(BookingAPI.as_view()), name="create-booking"
    ),
    re_path(
        r"^update-booking/(?P<pk>\d+)/$",
        login_required(BookingPutAPI.as_view()),
        name="update-booking",
    ),
]
